import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from "./component/login/login.component"
import { PatientsComponent } from "./component/patients/patients.component"
import { HealthcareComponent } from "./component/healthcare/healthcare.component"
import { SearchPatientsComponent } from "./component/search-patients/search-patients.component"
import { SeachRegisterComponent } from "./component/seach-register/seach-register.component"
import { PatientsRegisterComponent } from "./component/patients-register/patients-register.component"
import { PatientsImagesComponent } from "./component/patients-images/patients-images.component"
import { PatientsSignalsComponent } from "./component/patients-signals/patients-signals.component"

const routes: Routes = [
  {path: "", redirectTo: "ingreso", pathMatch: "full"},
  {path: "ingreso", component: LoginComponent},
  {path: "paciente/perfil", component: PatientsComponent}, 
  {path: "paciente/registros", component: PatientsRegisterComponent}, 
  {path: "paciente/señales", component: PatientsSignalsComponent}, 
  {path: "paciente/imagenes", component: PatientsImagesComponent}, 
  {path: "asistencial/perfil", component: HealthcareComponent}, 
  {path: "asistencial/guardar", component: SearchPatientsComponent}, 
  {path: "asistencial/consultar", component: SeachRegisterComponent}, 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
