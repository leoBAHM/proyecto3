import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './component/login/login.component';
import { PatientsComponent } from './component/patients/patients.component';
import { HealthcareComponent } from './component/healthcare/healthcare.component';
import { SearchPatientsComponent } from './component/search-patients/search-patients.component';
import { SeachRegisterComponent } from './component/seach-register/seach-register.component';
import { PatientsRegisterComponent } from './component/patients-register/patients-register.component';
import { PatientsSignalsComponent } from './component/patients-signals/patients-signals.component';
import { PatientsImagesComponent } from './component/patients-images/patients-images.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    PatientsComponent,
    HealthcareComponent,
    SearchPatientsComponent,
    SeachRegisterComponent,
    PatientsRegisterComponent,
    PatientsSignalsComponent,
    PatientsImagesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
