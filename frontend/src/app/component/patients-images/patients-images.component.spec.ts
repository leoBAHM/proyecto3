import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientsImagesComponent } from './patients-images.component';

describe('PatientsImagesComponent', () => {
  let component: PatientsImagesComponent;
  let fixture: ComponentFixture<PatientsImagesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PatientsImagesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientsImagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
