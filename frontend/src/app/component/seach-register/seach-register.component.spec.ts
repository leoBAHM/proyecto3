import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SeachRegisterComponent } from './seach-register.component';

describe('SeachRegisterComponent', () => {
  let component: SeachRegisterComponent;
  let fixture: ComponentFixture<SeachRegisterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SeachRegisterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeachRegisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
