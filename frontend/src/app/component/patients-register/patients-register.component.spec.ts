import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientsRegisterComponent } from './patients-register.component';

describe('PatientsRegisterComponent', () => {
  let component: PatientsRegisterComponent;
  let fixture: ComponentFixture<PatientsRegisterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PatientsRegisterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientsRegisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
