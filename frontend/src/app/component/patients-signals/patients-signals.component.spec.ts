import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientsSignalsComponent } from './patients-signals.component';

describe('PatientsSignalsComponent', () => {
  let component: PatientsSignalsComponent;
  let fixture: ComponentFixture<PatientsSignalsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PatientsSignalsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientsSignalsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
